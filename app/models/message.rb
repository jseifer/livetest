class Message < ActiveRecord::Base
  scope :unseen, where(seen: false)
end
