require 'livetest/sse'

class StreamsController < ApplicationController
  include ActionController::Live

  def messages
    response.headers['Content-Type'] = 'text/event-stream'

    sse = Livetest::SSE.new(response.stream)

    begin
      $redis.subscribe 'messages' do |on|
        on.message do |channel, data|
          sse.write ( data )
        end
      end
    rescue IOError
      # Client disconnect
    ensure
      sse.close
    end
  end

end
